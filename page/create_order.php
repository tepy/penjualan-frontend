<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container">
      <div class="row mb-2">
        <div class="col-sm-6">
          <!-- <h1 class="m-0 text-dark"> Top Navigation <small>Example 3.0</small></h1> -->
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="index?page=dashboard">Dashboard</a></li>
             <li class="breadcrumb-item"><a href="index?page=order">Order</a></li>
             <li class="breadcrumb-item title-func-order"><a href="javascript:void(0)">Tambah Order</a></li>
          </ol>
        </div>
      </div>
    </div>
  </div>

  <div class="content">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header">
              <h5 class="card-title m-0 title-div-order">Tambah Order</h5>
            </div>
            <div class="card-body">
              <form class="form-horizontal" id="formOrder" class="form-order">

                <div class="card-body">
                    <input type="hidden" name="idOrder" id="idOrder" value="">
                    <input type="hidden" name="valueOrder" id="valueOrder" class="form-order" value="insert">
                  <div class="form-group row">
                    <label for="buyer_name" class="col-sm-3 col-form-label">Nama Pembeli</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" name="buyer_name" id="buyer_name" placeholder="Nama Pembeli">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="order_number" class="col-sm-3 col-form-label">Nomor Order</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" name="order_number" id="order_number" placeholder="Nomor Order">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="order_number" class="col-sm-3 col-form-label">Tanggal</label>
                    <div class="col-sm-9">
                      <input type="date" class="form-control" name="date" id="date" placeholder="Nomor Order">
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="offset-sm-3 col-sm-9">
                      <div class="form-check">
                        
                      </div>
                      <button type="button" id="btnSaveCreateOrder" data-payload="insert" class="btn btn-md btn-primary">Simpan</button>
                    </div>
                  </div>
                </div>
            </div>
          </div>
        </div>

        <div class="col-lg-12" id="divDetailOrder" style="display: block;">
          <div class="card">
            <div class="card-header">
              <h5 class="card-title m-0">Detail Order</h5>
            </div>
            <div class="card-body">
              <button id="btnAddOrderDetail" class="btn btn-primary float-right">Tambah</button>
              <br><br>
              <table id="table-order-detail" class="table table-striped table-bordered" width="100%">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Barang</th>
                    <th>Harga</th>
                    <th>Qty</th>
                    <th>Subtotal</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
              </table>

            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modalOrderDetail">
  <div class="modal-dialog">
    <div class="modal-content">
      <form id="detail_order_form" class="order-detail">
        <div id="form_overlay" class="overlay d-flex justify-content-center align-items-center">
          <i class="fas fa-2x fa-sync fa-spin"></i>
        </div>
        <div class="modal-header">
          <h4 class="modal-title">Modal Order Detail</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <input type="hidden" name="idOrderDetail" id="idOrderDetail" class="form-item">
          <div class="form-group">
            <label for="">Nama Barang</label>
            <select name="item_id" id="item_id" class="form-control"></select>
            <div class="item_id invalid-feedback"></div>
          </div>
          <div class="form-group">
            <label for="">Jumlah</label>
            <input type="number" min="0" name="qty" id="qty" class="form-control">
            <div class="qty invalid-feedback"></div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" id="btnSubmitModalOrderDetail" class="btn btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>


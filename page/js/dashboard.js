$(document).ready(function() {
  
  $("#date_input_dashboard").daterangepicker({
    ranges  : {
      'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
      'Last 30 Days': [moment().subtract(29, 'days'), moment()],
      'Last 3 Months': [moment().subtract(3, 'months'), moment()],
    },
    alwaysShowCalendars: true,
    maxDate : moment().format('DD/MM/YYYY'),
    drops   : "down",
    locale: {
      format: "DD/MM/YYYY",
      separator: "|",
      applyLabel: "Apply",
      cancelLabel: "Cancel",
      fromLabel: "From",
      toLabel: "To",
      customRangeLabel: "Custom",
      weekLabel: "W",
      firstDay: 1
    }
  });
  $("#date_input_dashboard").val(moment().subtract(6, 'days').format('DD/MM/YYYY')+"|"+moment().format('DD/MM/YYYY'));
  statistic()
});

$(document).on('click', '#tbl-query-btn', function(event) {
  event.preventDefault();
  statistic()
});
function statistic() {
  if ($("#date_input_dashboard").val() != "") {
    $.ajax({
      url: APP_URL + 'statistik/get-data',
      type: 'POST',
      dataType: 'json',
      contentType: 'application/json',
      data: JSON.stringify({date:$("#date_input_dashboard").val()}),
      success: function(dt) {
        // console.log(dt)
        create_chart_divs("tbl");
        data_dump(dt);
      },
      fail: function(xhr, error, thrown) {
        console.log([xhr, error, thrown]);
        if (error == 'error' && xhr.responseJSON == undefined ) {
          alert('server tidak merespon')
        }
      }
    })
  }
  
}

function get_range_year(startDay = new Date(), endDay = new Date()) {
  var startDate = moment(startDay);
  var endDate = moment(endDay);
  var datesBetween = [];
  var startingMoment = startDate;

  while(startingMoment <= endDate) {
    datesBetween.push({
        'date': startingMoment.clone().format('DD MMM YYYY'),
        'val' : parseInt(startingMoment.clone().format('YYYYMMDD'))
      });
    startingMoment.add(1, 'days');
  }
  
  return datesBetween;
}

function data_dump(dt)
{
  var dtx = get_range_year(dt.data.date.start, dt.data.date.end);
  var val = [];
  dtx.forEach((item) => {
    val.push({"date":item.date, "val":item.val, "total":0});
  });
  dt.data.data.forEach((item) => {
    var index = val.findIndex(function(x){
      return(x.val == item.tgl);
    });
    if (index >= 0) {
      val[index].total = parseInt(item.total);
    }
  });
  var date = [];
  var total = [];
  val.forEach((item) => {
    date.push(item.date);
    total.push(item.total);
  });

  Highcharts.chart('tbl-chart', {

    title: {
        text: 'Statistic Penjualan'
    },

    xAxis: {
      categories: date,
      crosshair: true
    },
    yAxis: {
        title: {
            text: 'Range'
        }
    },

    plotOptions: {
      series: {
        dataLabels:{
          enabled : false,
        },
        allowPointSelect: false,
      }
    },
    series: [{
        name: 'Pendapatan',
        data: total,
    }],

    responsive: {
      rules: [{
        condition: {
            maxWidth: 500
        },
        chartOptions: {
            legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'bottom'
            }
        }
      }]
    }
  });

}

Highcharts.setOptions({
  lang: {
    numericSymbols: [" Ribu", " Juta", " Miliar", "Triliun", "Q", "Q"]
  },
});

function create_chart_divs(group) {
  $('.' + group + '.chart-container').remove();
  var ch = document.createElement('div');
  $(ch).attr('id', group + '-chart');
  $(ch).addClass(group);
  $(ch).addClass('chart-container');
  $(ch).css('min-width', '400px');
  $(ch).css('width', '100%');
  $(ch).css('height', '430px');
  $(ch).css('padding', '2px');
  $(ch).css('margin-top', '10px');
  $(ch).css('border', '1px solid #a0a0e0');
  $('#' + group + '-charts').append(ch);
}
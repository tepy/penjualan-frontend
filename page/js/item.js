$(document).ready(function() {
  $('#table-item').DataTable({
    responsive:true,
    serverSide:true,
    processing:true,
    ajax:{
      url: APP_URL + 'item/datatable',
      type: 'POST',
      dataType :'json',
      headers: {
        Authorization: 'bearer ' + localStorage.getItem('token'),
      },
      error: function(xhr, error, thrown){
        handleErrorDatatable(xhr, error, thrown);
      },
    },
    columns: [
      {data: 'id', orderable:false,searchable:false},
      {data: 'name'},
      {data: 'price'},
      {data: 'stock'},
      {data: 'act', orderable:false,searchable:false},
    ],
    
  });
});

$(document).on('click', '#btnAddItem', function(event) {
  event.preventDefault();
  $("#modalItem").modal('show');
  $("#formModalItem")[0].reset();
  loadingFormModal('off', 'formModalItem');
  method = "insert";
});

$(document).on('click', '.btn-delete.btn-datatable-item', function(event) {
  event.preventDefault();
  // 
  // console.log(id);
  // 
  Swal.fire({
    title: 'Apakah anda yakin?',
    text: "Anda akan menghapus data!",
    showCancelButton: true,
    confirmButtonColor: '#d33',
    cancelButtonColor: '#3085d6',
    confirmButtonText: 'Ya, hapus',
    cancelButtonText: 'tidak',
  }).then((result) => {
    console.log(result);
    if (result.value) {
      let id = $(this).data("payload");
      $.ajax({
        url: APP_URL + 'item/' + id + '/delete',
        type: 'POST',
        dataType: 'json',
        // data: {param1: 'value1'},
      })
      .done(function(dt) {
        Toast.fire({
          icon: 'success',
          title: ' Data berhasil dihapus'
        });
        $('#table-item').DataTable().ajax.reload(null, false);
      })
      .fail(function(xhr, error, thrown) {
        // console.log([xhr, error, thrown]);
        if (error == 'error' && xhr.responseJSON == undefined ) {
          alert('server tidak merespon')
        }
      });
    }
  });
  
});

function modalPrepareEdit(idModal, idFormModal){
  $("#" + idModal).modal('show');
  $("#" + idFormModal)[0].reset();
  loadingFormModal('off', idFormModal);
  method = "update";
}

$(document).on('click', '.btn-edit.btn-datatable-item', function(event) {
  event.preventDefault();

  let id = $(this).data("payload");
  console.log(id);
  $.ajax({
    url: APP_URL + 'item/' + id + '/show',
    type: 'GET',
    dataType: 'json',
    // data: {param1: 'value1'},
  })
  .done(function(dt) {
    modalPrepareEdit("modalItem", "formModalItem");
    method = 'update';
    $("#idItem").val(dt.data.id);
    $("#name").val(dt.data.name);
    $("#price").val(parseInt(dt.data.price));
    $("#stock").val(dt.data.stock);
  })
  .fail(function(xhr, error, thrown) {
    handleErrorDatatable(xhr, error, thrown)
  });
});

$(document).on('submit', '#formModalItem', function(event) {
  event.preventDefault();
  loadingFormModal('on', 'formModalItem');
  let url;
  if (method == 'insert') {
    url =  APP_URL + 'item/create';
  } else {
    url =  APP_URL + 'item/' + $("#idItem").val() + '/update';
  }
  $.ajax({
    url: url,
    type: 'POST',
    dataType: 'json',
    processData: false,
    contentType: false,
    cache: false,
    data: new FormData(this),
    headers: {
      Authorization: 'bearer ' + localStorage.getItem('token'),
    },
  })
  .done(function(dt) {
    if (dt.code == 0) {
      loadingFormModal("off", "modalItem");
      $("#modalItem").modal("hide");
      Toast.fire({
        icon: 'success',
        title: ' Data berhasil disimpan'
      });
      $('#table-item').DataTable().ajax.reload(null, false);
    }
  })
  .fail(function(xhr, error, thrown) {
    loadingFormModal('off', 'formModalItem');
    if (xhr.status == 402) {
      var errors = xhr.responseJSON;
      for(var key in errors) {
        if (!errors.hasOwnProperty(key)) continue;
        $(`input[name="${key}"]`).addClass('is-invalid')
        $(`.${key}`).text(errors[key]['0']);
      }
    } else if (error == 'error' && xhr.responseJSON == undefined ) {
      alert('server tidak merespon')
    }
  });
  
});


function loadingFormModal(method = "on", idForm) {
  if (method == "on") {
    $('#form_overlay').addClass('d-flex')
    $('#form_overlay').show();
  } else {
    $('#form_overlay').removeClass('d-flex')
    $('#form_overlay').hide();
  }
  $('#' + idForm).find('.is-invalid').removeClass('is-invalid');
}

function stopLoadingFormModal() {
  $('#form_overlay').removeClass('d-flex')
  $('#form_overlay').hide();
}

function removeErrorFormModal(idForm = 'form') {
  $('#' + idForm).find('.is-invalid').removeClass('is-invalid');
}

// function loadingFormModal() {
//   $('#form_overlay').addClass('d-flex')
//   $('#form_overlay').show();
// }

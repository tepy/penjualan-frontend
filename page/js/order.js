$(document).ready(function() {
  
  $("#table-order").DataTable({
    responsive: true,
    serverSide: true,
    processing: true,
    ajax: {
      url: APP_URL + 'order/datatable',
      type: 'POST',
      dataType :'json',
    },
    error: function(xhr, error, thrown) {
      handleErrorDatatable(xhr, error, thrown);
    },
    columns:[
      {data: 'id',orderable:false,searchable:false},
      {data: 'buyer_name'},
      {data: 'order_number'},
      {data: 'date'},
      {data: 'act',orderable:false,searchable:false},
    ]
  });
});

$(document).on('click', '.btn-edit.btn-datatable-order', function(event) {
  event.preventDefault();
  let id = $(this).data("payload");
  window.location = 'index?page=create-order&action=update&id=' + id;
});

$(document).on('click', '.btn-delete.btn-datatable-order', function(event) {
  event.preventDefault();
  Swal.fire({
    title: 'Apakah anda yakin?',
    text: "Anda akan menghapus data!",
    showCancelButton: true,
    confirmButtonColor: '#d33',
    cancelButtonColor: '#3085d6',
    confirmButtonText: 'Ya, hapus',
    cancelButtonText: 'tidak',
  }).then((result) => {
    if (result.value) {
      let id = $(this).data("payload");
      $.ajax({
        url: APP_URL + 'order/' + id + '/delete',
        type: 'POST',
        dataType: 'json',
      })
      .done(function(dt) {
        Toast.fire({
          icon: 'success',
          title: ' Data berhasil dihapus'
        });
        $('#table-order').DataTable().ajax.reload(null, false);
      })
      .fail(function(xhr, error, thrown) {
        // console.log([xhr, error, thrown]);
        if (error == 'error' && xhr.responseJSON == undefined ) {
          alert('server tidak merespon')
        }
      });
    }
  });
});
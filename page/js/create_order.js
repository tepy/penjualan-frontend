
$(document).ready(function() {
  let newURL = new URL(window.location.protocol + "//" + window.location.host + "" + window.location.pathname + window.location.search);
  let action = newURL.searchParams.get("action");
  
  if (action == 'update') {
    console.log(true)
    let id = newURL.searchParams.get("id");
    $.ajax({
      url : APP_URL + 'order/' + id + '/show',
      dataType: 'json',
      contentType: 'application/json',
      success: function(dt){
        console.log(dt)
        if (dt.code == 0) {
          $('#idOrder').val(dt.data.id);
          $('#valueOrder').val('update');
          $('#buyer_name').val(dt.data.buyer_name);
          $('#order_number').val(dt.data.order_number);
          $('#date').val(dt.data.date);
          $('#btnSaveCreateOrder').text('update');
          $('.title-func-order').text('Update Order');
          $('.card-title.m-0.title-div-order').text('Update Order');
          $('#table-order-detail').DataTable().ajax.reload(null, false);
        }
      },
      error: function(xhr, error, thrown) {
        console.log([xhr, error, thrown]);
        if (error == 'error' && xhr.responseJSON == undefined ) {
          alert('server tidak merespon')
        }
      }
    })
  }
  
  $('#table-order-detail').DataTable({
    responsive: true,
    serverSide: true,
    processing: true,
    ajax: {
      url: APP_URL + 'order-detail/datatable',
      type: 'POST',
      dataType: 'json',
      data: function(x) {
        if ($("#idOrder").val() == '') {
          x.order_id = 0;
        } else {
          x.order_id = $("#idOrder").val();
        }
      },
    },
    error: function(xhr, error, thrown) {
      console.log([xhr, error, thrown]);
    },
    columns:[
      {data: 'id', orderable:false, searchable:false},
      {data: 'name'},
      {data: 'price'},
      {data: 'qty'},
      {data: 'subtotal'},
      {data: 'act', orderable:false, searchable:false},
    ]
  });

});




$(document).on('click', '#btnSaveCreateOrder', function(event) {
  event.preventDefault();
  let url, notif;
  if ($('#valueOrder.form-order').val() == 'insert') {
    url = APP_URL + 'order/create';
    notif = ' Data berhasil disimpan';
  } else {
    url = APP_URL + 'order/' + $('#idOrder').val() + '/update';
    notif = ' Data berhasil diubah';
  }
  $.ajax({
    url: url,
    type: 'POST',
    dataType: 'json',
    processData: false,
    contentType: false,
    cache: false,
    data: new FormData($('#formOrder')[0]),
  })
  .done(function(dt) {
    console.log(dt);
    if (dt.code == 0) {
      Toast.fire({
        icon: 'success',
        title: notif
      });
      $('#formOrder').find('.is-invalid').removeClass('is-invalid');
      $("#divDetailOrder").css('display', 'block');
      $("#idOrder").val(dt.data.id);
      $("#valueOrder").val("update");
      $("#btnSaveCreateOrder").text('Ubah')
      $('#table-order-detail').DataTable().ajax.reload();
    }
  })
  .fail(function(xhr, error, thrown) {
    console.log([xhr, error, thrown]);
    $('#formOrder').find('.is-invalid').removeClass('is-invalid');
    if (xhr.status == 402) {
      var errors = xhr.responseJSON;
      for(var key in errors) {
        if (!errors.hasOwnProperty(key)) continue;
        $(`input[name="${key}"]`).addClass('is-invalid')
        $(`.${key}`).text(errors[key]['0']);
      }
    } else if (error == 'error' && xhr.responseJSON == undefined ) {
      alert('server tidak merespon')
    }
  });
  
});






$(document).on('click', '#btnAddOrderDetail', function(event) {
  event.preventDefault();
    $('#qty').val('');
    $("#modalOrderDetail").modal('show');
    loadingFormModal('off', "detail_order_form");
    method = 'insert';
    getListItem();
});

$(document).on('click', '.btn-edit.btn-datatable-order-item', function(event) {
  event.preventDefault();
  let id = $(this).data('payload');
  $.ajax({
    url: APP_URL + 'order-detail/' + id + '/show',
    type:'GET',
    dataType: 'json',
    contentType: 'application/json',
    success: function(dt){
      console.log(dt);
      if (dt.code == 0) {       
        loadingFormModal('off', "detail_order_form");
        method = 'update'
        $('#idOrderDetail').val(dt.data.id);
        $('#qty').val(dt.data.qty);
        getListItem(dt.data.item_id);
        $('#modalOrderDetail').modal('show');
      }
    },
    fail: function(xhr, error, thrown){
      console.log([xhr, error, thrown]);
      if (error == 'error' && xhr.responseJSON == undefined ) {
        alert('server tidak merespon')
      }
    },
  });
});


$(document).on('click', '#btnSubmitModalOrderDetail', function(event) {
  event.preventDefault();
  let url, notif;
  if (method == 'insert') {
    url = APP_URL + 'order-detail/create'; 
    notif = 'Data berhasil disimpan';
  } else {
    url = APP_URL + 'order-detail/' + $('#idOrderDetail').val() + '/update';
    notif = 'Data berhasil diubah';
  }
  loadingFormModal('on', "detail_order_form");
  $.ajax({
    url: url,
    type: 'POST',
    dataType: 'json',
    processData: false,
    cache: false,
    contentType: 'application/json',
    data: JSON.stringify({
      order_id : $('#idOrder').val(),
      item_id : $('#item_id option:selected').val(),
      qty : $('#qty').val(),
    }),
  })
  .done(function(dt) {   
    if (dt.code == 0) {
      $('#qty').val('');
      Toast.fire({
        icon: 'success',
        title: notif
      });
      $('#modalOrderDetail').modal('hide');
      loadingFormModal('off', "detail_order_form");
      $('#table-order-detail').DataTable().ajax.reload(null, false);
    }
  })
  .fail(function(xhr, error, thrown) {
    console.log([xhr, error, thrown]);
    loadingFormModal('off', "detail_order_form");
    if (xhr.status == 402) {
      var errors = xhr.responseJSON;
      for(var key in errors) {
        if (!errors.hasOwnProperty(key)) continue;
        $(`input[name="${key}"]`).addClass('is-invalid')
        $(`select[name="${key}"]`).addClass('is-invalid')
        $(`.${key}`).text(errors[key]['0']);
      }
    } else if (error == 'error' && xhr.responseJSON == undefined ) {
      alert('server tidak merespon')
    }
  });
  

});



$(document).on('click', '.btn-delete.btn-datatable-order-item', function(event) {
  event.preventDefault();

  Swal.fire({
    title: 'Apakah anda yakin?',
    text: "Anda akan menghapus data!",
    showCancelButton: true,
    confirmButtonColor: '#d33',
    cancelButtonColor: '#3085d6',
    confirmButtonText: 'Ya, hapus',
    cancelButtonText: 'tidak',
  }).then((result) => {
    if (result.value) {
      let id = $(this).data("payload");
      $.ajax({
        url: APP_URL + 'order-detail/' + id + '/delete',
        type: 'POST',
        dataType: 'json',
      })
      .done(function(dt) {
        Toast.fire({
          icon: 'success',
          title: ' Data berhasil dihapus'
        });
        $('#table-order-detail').DataTable().ajax.reload(null, false);
      })
      .fail(function(xhr, error, thrown) {
        console.log([xhr, error, thrown]);
        if (error == 'error' && xhr.responseJSON == undefined ) {
          alert('server tidak merespon')
        }
      });
    }
  });
});

function getListItem(id_item = null, on_done = null){
  $.ajax({
    url: APP_URL + 'item/list-item',
    type: 'GET',
    dataType: 'json',
  })
  .done(function(dt) {
    console.log(dt);
    if (dt.code == 0) {
      $('select#item_id').html('');
      dt.data.forEach((item) => {
        $('select#item_id').append(new Option(item.name, item.id))
      });
      if (id_item != null) {
        $('select#item_id').val(id_item);
      }
      if (on_done != null) {
        on_done();
      }
    }
  })
  .fail(function(xhr, error, thrown) {
    console.log([xhr, error, thrown]);
  });
  
}

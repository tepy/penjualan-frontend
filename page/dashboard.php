<div class="content-wrapper">
  <div class="content-header">
    <div class="container">
      <div class="row mb-2">
        <div class="col-sm-6">
          <!-- <h1 class="m-0 text-dark"> Top Navigation <small>Example 3.0</small></h1> -->
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="index.php?page=dashboard">Dashboard</a></li>
            <!-- <li class="breadcrumb-item"><a href="#">Layout</a></li>
            <li class="breadcrumb-item active">Top Navigation</li> -->
          </ol>
        </div>
      </div>
    </div>
  </div>

  <div class="content">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header">
              <h5 class="card-title m-0">Statistik</h5>
            </div>
            <div class="card-body">
              
              <div id="tbl-charts" class="box-body">
                
                <div style="background-color: #e2e2ff; padding: 3px; border: 1px solid #c0c0e0;">
                  <table width="100%" border="0" style="border-collapse: collapse;">
                    <tr>
                      <td>
                        <div style="padding-top: 3px;">
                          <label>Range Date</label> &nbsp; 
                          <input id="date_input_dashboard" name="date_input_dashboard">
                          
                        </div>
                      </td>
                      <td align="right">
                        <button id="tbl-query-btn" class="tbl query bttn btn-sm">Show</button>
                      </td>
                    </tr>
                  </table>
                </div>

              </div>

            </div>
          </div>

          
        </div>
      </div>
    </div>
  </div>
</div>
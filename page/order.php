<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container">
      <div class="row mb-2">
        <div class="col-sm-6">
          <!-- <h1 class="m-0 text-dark"> Top Navigation <small>Example 3.0</small></h1> -->
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="index?page=dashboard">Dashboard</a></li>
             <li class="breadcrumb-item"><a href="index?page=order">Order</a></li>
            <!--<li class="breadcrumb-item active">Top Navigation</li> -->
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <div class="content">
    <div class="container">
      <div class="row">
       <!-- /.col-md-6 -->
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header">
              <h5 class="card-title m-0">Order</h5>
            </div>
            <div class="card-body">
              <a href="index?page=create-order"><button id="btnAddOrder" class="btn btn-primary float-right">Add</button></a>
              <br><br>
              <table id="table-order" class="table table-striped table-bordered" width="100%">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama pembeli</th>
                    <th>No. Order</th>
                    <th>Tanggal</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                
              </table>

            </div>
          </div>

          
        </div>
        <!-- /.col-md-6 -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content -->
</div>


<div class="modal fade" id="modalOrder">
  <div class="modal-dialog">
    <form id="formModalItem" class="classModalOrder">
      <div class="modal-content">
        <div id="form_overlay" class="overlay d-flex justify-content-center align-items-center">
              <i class="fas fa-2x fa-sync fa-spin"></i>
          </div>
        <div class="modal-header">
          <h4 class="modal-title">Default Modal</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <input type="hidden" name="idOrder" id="idOrder" class="form-item">
          <div class="form-group">
            <label for="">Nama</label>
            <input type="text" name="name" id="name" class="form-control">
            <div class="name invalid-feedback"></div>
          </div>
          <div class="form-group">
            <label for="">Harga</label>
            <input type="number" min="0" name="price" id="price" class="form-control">
            <div class="price invalid-feedback"></div>
          </div>
          <div class="form-group">
            <label for="">Stok</label>
            <input type="number" min="0" name="stock" id="stock" class="form-control">
            <div class="stock invalid-feedback"></div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
      </div>
    </form>
  </div>
</div>
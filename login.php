<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Login</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Sign in to start your session</p>

      <form id="form-login">
        <div class="input-group mb-3">
          <input type="email" name="email" id="email" required="" class="form-control" placeholder="Email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" name="password" id="password" required="" class="form-control" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<script type="text/javascript" src="general.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    // console.log(APP_URL);

  });

  $(document).on('submit', '#form-login', function(event) {
    event.preventDefault();
    let data = new FormData(this);
    localStorage.removeItem('token');
    // localStorage.clear();
    // console.log(data)
    $.ajax({
      url: APP_URL + 'login',
      type: 'POST',
      dataType: 'json',
      processData: false,
      contentType: false,
      cache: false,
      data: data,
    })
    .done(function(dt) {
      console.log(dt);
      // dt.data.token
      // localStorage.clear();

      localStorage.setItem('token', dt.token);
      window.location = 'index';
    })
    .fail(function(xhr, error, thrown) {
      console.log([xhr, error, thrown]);
      if (xhr.status == 402) {
        var errors = xhr.responseJSON;
        for(var key in errors) {
          if (!errors.hasOwnProperty(key)) continue;
          $(`input[name="${key}"]`).addClass('is-invalid')
          $(`.${key}`).text(errors[key]['0']);
        }
      } else if (xhr.status == 401) {
        alert(xhr.responseJSON.desc);
        $('#form-login')[0].reset();
        $('#email').focus();
      }
    });
    
  });
</script>

</body>
</html>

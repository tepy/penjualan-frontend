const APP_URL = 'http://localhost:8080/api/';
// const APP_URL = 'http://njajal.ku/penjualan-backend/api/';

$(document).ready(function() {
  cekToken();
});

  $.ajaxSetup({
    headers: {
      Authorization: 'bearer ' + localStorage.getItem('token'),
    },
  });

function cekToken() {
    let cek = localStorage.getItem('token');
    if ( cek == null && !location.pathname.includes("login")) {
      window.location = 'login';
    } else {
      $.ajax({
        url: APP_URL + 'get-user',
        type: 'GET',
        dataType: 'json',
      })
      .done(function(dt) {
        // console.log(dt);
      })
      .fail(function(xhr, error, thrown) {
        console.log([xhr, error, thrown]);
        if (thrown == 'Unauthorized') {
          localStorage.clear();
        } else if (error == 'error' && xhr.responseJSON == undefined ) {
          alert('server tidak merespon')
        }
      });
      
    }
}

function getGlobalUser(on_done = null){
  let status = false;
  $.getJSON(APP_URL + 'get-user', function(json, textStatus, i) {
    status = true;
    if (on_done!= null) {
      on_done(status);
    }
  });
}

function handleErrorDatatable(xhr, error, thrown) {
  console.log([xhr, error, thrown])
  if (xhr.status == 401) {
    if (xhr.responseText == 'Unauthorized.') {
      localStorage.removeItem('token');
      window.location = 'login';
    }
  }
}

$(document).on('click', 'a.dropdown-item.log-out', function(event) {
  event.preventDefault();

  fetch(APP_URL + 'logout', {
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      Authorization: 'bearer ' + localStorage.getItem('token'),
    },
    method: "POST",
  })
  .then(response => {    
    if (response.ok) {
      return response.json();
    } else {
      return Promise.reject({
        status: response.status,
        statusText: response.statusText
      });
    }
  })
  .then(x => {
    localStorage.removeItem('token');
    window.location = 'login';
  })
  .catch(error => {
    console.log(error);
  });

});

$(document).on('click', 'a.nav-link.btn-optimasi-db', function(event) {
  event.preventDefault();
  $.ajax({
    url: APP_URL + 'optimasi-database',
    type: 'POST',
    dataType: 'json',
    contentType: 'application/json',
  })
  .done(function(dt) {
    let data = JSON.stringify(dt, undefined, 4);
    $('#responOptimize').text(data);
    $('#modalOptimasiDatabase').modal('show');
  })
  .fail(function(xhr, error, thrown) {
    console.log([xhr, error, thrown]);
  });
  
});